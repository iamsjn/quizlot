﻿using QuizLot.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace QuizLot.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        QuizLotEntities db = new QuizLotEntities();

        [AllowAnonymous]
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                string username = HttpContext.User.Identity.Name.Split('|')[1].ToString();
                return RedirectToAction("Index", username);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult SignUp(users data, string repassword, string gender)
        { 
                if (!ModelState.IsValid)
                {
                    Session["signup-error"] = 1;
                    return View("Index");
                }

                var username = db.users.Where(u => u.user_name == data.user_name).Any();
                var email = db.users.Where(e => e.email == data.email).Any();

                if (username == false && email == false && data.password == repassword && gender != null)
                {
                    data.date = DateTime.Now;
                    db.users.Add(data);
                    db.SaveChanges();

                    var userinfo = db.users.Where(u => u.user_name == data.user_name && u.password == data.password).Select(n => new { n.id, n.user_name }).FirstOrDefault();
                    FormsAuthentication.SetAuthCookie(userinfo.id + "|" + userinfo.user_name, false);

                    return RedirectToAction("Index", userinfo.user_name);
                }
                else
                {
                    Session["signup-error"] = 1;
                    if (username != false && email != false && data.password != repassword && gender == null)
                    {
                        ViewBag.username_error = "This username is not available";
                        ViewBag.email_error = "This email is not available";
                        ViewBag.pass_error = "Password doesn't match";
                        ViewBag.gender_error = "Select your gender";
                        return View("Index");
                    }
                    else if (username != false && email != false && data.password != repassword)
                    {
                        ViewBag.username_error = "This username is not available";
                        ViewBag.email_error = "This email is not available";
                        ViewBag.pass_error = "Password doesn't match";
                        return View("Index");
                    }
                    else if (username != false && email != false && gender == null)
                    {
                        ViewBag.username_error = "This username is not available";
                        ViewBag.email_error = "This email is not available";
                        ViewBag.gender_error = "Select your gender";
                        return View("Index");
                    }
                    else if (username != false && data.password != repassword && gender == null)
                    {
                        ViewBag.username_error = "This username is not available";
                        ViewBag.pass_error = "Password doesn't match";
                        ViewBag.gender_error = "Select your gender";
                        return View("Index");
                    }
                    else if (email != false && data.password != repassword && gender == null)
                    {
                        ViewBag.email_error = "This email is not available";
                        ViewBag.pass_error = "Password doesn't match";
                        ViewBag.gender_error = "Select your gender";
                        return View("Index");
                    }
                    else if (username != false && email != false)
                    {
                        ViewBag.username_error = "This username is not available";
                        ViewBag.email_error = "This email is not available";
                        return View("Index");
                    }
                    else if (username != false && gender == null)
                    {
                        ViewBag.username_error = "This username is not available";
                        ViewBag.gender_error = "Select your gender";
                        return View("Index");
                    }
                    else if (username != false && data.password != repassword)
                    {
                        ViewBag.username_error = "This username is not available";
                        ViewBag.pass_error = "Password doesn't match";
                        return View("Index");
                    }
                    else if (email != false && data.password != repassword)
                    {
                        ViewBag.email_error = "This email is not available";
                        ViewBag.pass_error = "Password doesn't match";
                        return View("Index");
                    }
                    else if (email != false && gender == null)
                    {
                        ViewBag.email_error = "This email is not available";
                        ViewBag.gender_error = "Select your gender";
                        return View("Index");
                    }
                    else if (data.password != repassword && gender == null)
                    {
                        ViewBag.pass_error = "Password doesn't match";
                        ViewBag.gender_error = "Select your gender";
                        return View("Index");
                    }
                    else if (username != false)
                    {
                        ViewBag.username_error = "This username is not available";
                        return View("Index");
                    }
                    else if (email != false)
                    {
                        ViewBag.email_error = "This email is not available";
                        return View("Index");
                    }
                    else if (data.password != repassword)
                    {
                        ViewBag.pass_error = "Password doesn't match";
                        return View("Index");
                    }
                    else
                    {
                        ViewBag.gender_error = "Select your gender";
                        return View("Index");
                    }
                }
        }

        [HttpPost]
        public ActionResult SignIn(string username, string password)
        {
            var user = db.users.Where(u => u.user_name == username && u.password == password).Any();

            if (user == true)
            {
                var userinfo = db.users.Where(u => u.user_name == username && u.password == password).Select(n => new { n.id, n.user_name }).FirstOrDefault();
                FormsAuthentication.SetAuthCookie(userinfo.id + "|" + userinfo.user_name, false);

                return RedirectToAction("Index", userinfo.user_name);
            }
            else
            {
                ViewBag.signin_error = "Username and password combination doesn't match.";
                return View("Index");
            }
        }

        [HttpGet]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}
