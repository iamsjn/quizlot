﻿using Owin;
using Microsoft.Owin;
using QuizLot.Hubs;
using Microsoft.AspNet.SignalR;
[assembly: OwinStartup(typeof(QuizLot.Startup))]
namespace QuizLot
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var idProvider = new CustomUserIdProvider();

            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => idProvider);
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}