/*
  Flat - v.2.0.0
  http://haundodesign.com/flat
  
  Handcrafted by Haundo Design
*/

$(document).ready(function () {
  $('.nav>li').click(function (e) {
    $('.nav>li').removeClass('active');

    if(!$(this).hasClass('active')) {
      $(this).addClass('active');
    }
    e.preventDefault();
  });
});