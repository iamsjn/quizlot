﻿using QuizLot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace QuizLot
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Home",
                url: "Home/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Getquestion",
                url: "quiz/{username}/getquestion",
                defaults: new { controller = "Quiz", action = "Getquestion" }
            );

            routes.MapRoute(
                name: "Quiz",
                url: "Quiz/{username}/{subjectname}",
                defaults: new { controller = "Quiz", action = "Index"}
            );

            routes.MapRoute(
                name: "TopicFind",
                url: "Topic/FindTopic",
                defaults: new { controller = "Topic", action = "FindTopic" }
            );

            routes.MapRoute(
                name: "PeopleFind",
                url: "Topic/FindPeople",
                defaults: new { controller = "Topic", action = "FindPeople" }
            );

            routes.MapRoute(
                name: "TopicCheck",
                url: "Topic/CheckTopic",
                defaults: new { controller = "Topic", action = "CheckTopic" }
            );

            routes.MapRoute(
                name: "GetNoti",
                url: "profile/getnoti",
                defaults: new { controller = "Profile", action = "GetNoti" }
            );

            routes.MapRoute(
                name: "GetNoti2",
                url: "topic/getnoti",
                defaults: new { controller = "Topic", action = "GetNoti" }
            );

            routes.MapRoute(
                name: "GetFriendNoti",
                url: "profile/getfriendnoti",
                defaults: new { controller = "Profile", action = "GetFriendNoti" }
            );

            routes.MapRoute(
                name: "AddFriend",
                url: "profile/addfriend",
                defaults: new { controller = "Profile", action = "AddFriend" }
            );
            
            routes.MapRoute(
                name: "DeleteNoti",
                url: "profile/deletenoti",
                defaults: new { controller = "Profile", action = "DeleteNoti" }
            );

            routes.MapRoute(
                name: "DeleteNoti2",
                url: "topic/deletenoti",
                defaults: new { controller = "Topic", action = "DeleteNoti" }
            );

            routes.MapRoute(
                name: "QuizInitialize",
                url: "quiz/quizinitialize",
                defaults: new { controller = "Quiz", action = "QuizInitialize" }
            ); 

            routes.MapRoute(
                name: "Topic",
                url: "Topic/{opponentname}",
                defaults: new { controller = "Topic", action = "Index" }
            );

                routes.MapRoute(
                    name: "UserProfile",
                    url: "{username}",
                    defaults: new { controller = "Profile", action = "Index" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}