﻿using QuizLot.Models;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace QuizLot.Controllers
{
    public class ProfileController : Controller
    {
        //
        // GET: /Profile/

        QuizLotEntities db = new QuizLotEntities();
        string[] sAllowedExt = new string[] { ".jpg", ".gif", ".png" };

        [AllowAnonymous]
        public ActionResult Index(string username)
        {
            var user = db.users.Where(u => u.user_name == username).Any();
            if (user == true)
            {
                dynamic mymodel = new ExpandoObject();
                mymodel.users = db.users.Where(u => u.user_name == username).ToList();
                var userid = db.users.Where(u => u.user_name == username).Select(n => n.id).FirstOrDefault();

                var usersubjectId = db.user_favorite_subjects.Where(u => u.user_id == userid).ToList();
                List<subjects> templist = new List<subjects>();
                List<subjects> subjectlist = new List<subjects>();
                
                foreach (var sid in usersubjectId)
                {
                    templist = db.subjects.Where(i => i.id == sid.subject_id).ToList();
                    subjectlist.AddRange(templist);
                }

                mymodel.subjects = subjectlist;

                var userfriendId = db.user_friends.Where(u => u.user_id == userid).ToList();
                List<users> templist2 = new List<users>();
                List<users> friendlist = new List<users>();

                foreach (var fid in userfriendId)
                {
                    templist2 = db.users.Where(i => i.id == fid.user_friend_id).ToList();
                    friendlist.AddRange(templist2);
                }

                mymodel.user_friends = friendlist;

                mymodel.user_quizes = db.user_quizes.Where(i => i.user_id == userid).ToList();

                return View(mymodel);
            }
            else
            {
               return HttpNotFound();
            }
        }

        [HttpPost][Authorize]
        public ActionResult Index(IEnumerable<HttpPostedFileBase> file)
        {
                foreach (var f in file)
                {
                    if (f != null && f.ContentLength > 0 && sAllowedExt.Contains(f.FileName.Substring(f.FileName.LastIndexOf('.'))))
                    {
                        string extension = Path.GetExtension(f.FileName);
                        var fileName = HttpContext.User.Identity.Name.Split('|')[1].ToString() + extension;
                        var path = Path.Combine(Server.MapPath("~/Content/Profile_Pictures"), fileName);
                        f.SaveAs(path);

                        int userId = Convert.ToInt32(HttpContext.User.Identity.Name.Split('|')[0]);
                        users user = new users();
                        var user_info = db.users.Where(i => i.id == userId).FirstOrDefault();
                        user_info.pro_pic_location = "/Content/Profile_Pictures/" + fileName;
                        db.SaveChanges();
                    }
                    else
                    {
                        ViewBag.PropicError = "Invalid Photo";
                    }
                }

                return RedirectToAction("Index", HttpContext.User.Identity.Name.Split('|')[1].ToString());
        }

        [Authorize]
        public ActionResult FindPeople(string term)
        {
            return Json(db.users.Where(u => u.user_name.StartsWith(term) || u.first_name.StartsWith(term) || u.last_name.StartsWith(term)).Select(n => n.user_name).ToList(), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetNoti(string receiverName)
        {
            return Json(db.user_notifications.Where(r => r.receiver_name == receiverName).OrderByDescending(d => d.date).ToList(), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetFriendNoti(string receiverName)
        {
            return Json(db.user_notifications.Where(r => r.receiver_name == receiverName || r.user_name == receiverName).ToList(), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult AddFriend(string username, string receivername, string notitype)
        {
            byte type = Convert.ToByte(notitype);
            var user_id = db.users.Where(u => u.user_name == username).Select(i => i.id).FirstOrDefault();
            var receiver_id = db.users.Where(u => u.user_name == receivername).Select(i => i.id).FirstOrDefault();

            if (db.user_friends.Where(u => u.user_id == user_id && u.user_friend_id == receiver_id || u.user_id == receiver_id && u.user_friend_id == u.user_id).Any())
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var itemToRemove = db.user_notifications.SingleOrDefault(n => n.user_name == username && n.receiver_name == receivername && n.type == type);
                db.user_notifications.Remove(itemToRemove);

                user_friends frnd = new user_friends();
                frnd.user_id = user_id;
                frnd.user_friend_id = receiver_id;
                frnd.date = DateTime.Now;
                db.user_friends.Add(frnd);
                db.SaveChanges();

                frnd.user_id = receiver_id;
                frnd.user_friend_id = user_id;
                frnd.date = DateTime.Now;
                db.user_friends.Add(frnd);
                db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public ActionResult DeleteNoti(string username, string receivername, string notitype)
        {
            byte type = Convert.ToByte(notitype);

            if (db.user_notifications.Where(u => u.user_name == username && u.receiver_name == receivername && u.type == type).Any())
            {
                var itemToRemove = db.user_notifications.SingleOrDefault(n => n.user_name == username && n.receiver_name == receivername && n.type == type);
                db.user_notifications.Remove(itemToRemove);
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

        }

        
    }
}
