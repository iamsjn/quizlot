﻿using QuizLot.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuizLot.Controllers
{
    public class QuizController : Controller
    {
        QuizLotEntities db = new QuizLotEntities();
        //
        // GET: /Quiz/

        [AllowAnonymous]
        public ActionResult Index(string userName, string subjectName)
        {
            if (!Request.IsAuthenticated)
            {
                return HttpNotFound();
            }

            else
            {
                if (db.users.Where(u => u.user_name == userName).Any() && db.subjects.Where(s => s.name == subjectName).Any())
                {
                    dynamic mymodel = new ExpandoObject();

                    int userId = db.users.Where(i => i.user_name == userName).Select(i => i.id).FirstOrDefault();
                    int opponent_id = Convert.ToInt32(HttpContext.User.Identity.Name.Split('|')[0]);

                    mymodel.temp_quiz_informations = db.temp_quiz_informations.Where(ques => ques.user_id == userId && ques.opponent_id == opponent_id && ques.subject_name == subjectName && ques.question_serial_number == 1).Take(1).ToList();
                    mymodel.users = db.users.Where(u => u.id == opponent_id).ToList();

                    string opponentPic = db.users.Where(p => p.id == userId).Select(p => p.pro_pic_location).SingleOrDefault();
                    if (opponentPic == null)
                    {
                        ViewBag.opponentPic = "/Content/Profile_Pictures/default_profile.png";
                    }
                    else
                    {
                        ViewBag.opponentPic = opponentPic;
                    }

                    return View(mymodel);

                }
                else
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpPost][Authorize]
        public ActionResult Getquestion(int questionId, string userId, string topicName)
        {
            int user_id = Convert.ToInt32(userId);
            int opponent_id = Convert.ToInt32(HttpContext.User.Identity.Name.Split('|')[0]);
            return Json(db.temp_quiz_informations.Where(ques => ques.user_id == user_id && ques.opponent_id == opponent_id && ques.subject_name == topicName && ques.question_serial_number == questionId).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost][Authorize]
        public ActionResult CheckAnswer(int id, string option)
        {
            var check_ans = db.temp_quiz_informations.Where(u => u.user_id == 1 && u.opponent_id == 2 && u.subject_name == "Test" && u.correct_answer == option).Any();

            if(check_ans == true)
            {
                return Json("Coreect", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Incorrect", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost][Authorize]
        public ActionResult QuizInitialize(string userName, string opponentName, string topicName)
        {
            var checkUser = db.users.Where(u => u.user_name == userName).Any();
            var checkOpponent = db.users.Where(u => u.user_name == opponentName).Any();
            var checkSubject = db.subjects.Where(s => s.name == topicName).Any();

            if (db.user_notifications.Where(n => n.user_name == userName && n.receiver_name == opponentName && n.type == 1).Any() && checkUser && checkOpponent)
            {
                var itemToRemove = db.user_notifications.SingleOrDefault(n => n.user_name == userName && n.receiver_name == opponentName && n.type == 1);
                db.user_notifications.Remove(itemToRemove);
                db.SaveChanges();

                if (checkSubject)
                {
                    int userId = db.users.Where(i => i.user_name == userName).Select(i => i.id).FirstOrDefault();
                    int subjectId = db.subjects.Where(i => i.name == topicName).Select(i => i.id).FirstOrDefault();

                    temp_quiz_informations obj = new temp_quiz_informations();
                    int ques_serial = 1;
                    var questionlist = db.questions.Where(ques => ques.subject_id == subjectId).OrderBy(r => Guid.NewGuid()).Take(4).ToList();

                    foreach (var item in questionlist)
                    {
                        obj.user_id = userId;
                        obj.opponent_id = Convert.ToInt32(HttpContext.User.Identity.Name.Split('|')[0]);
                        obj.question_serial_number = ques_serial;
                        obj.subject_name = topicName;
                        obj.question_name = item.name;
                        obj.question_option = item.options;
                        obj.correct_answer = item.correct_answer;
                        db.temp_quiz_informations.Add(obj);

                        db.SaveChanges();
                        ques_serial++;
                    }

                    return Json(true);
                }
                else
                {
                    return Json(false);
                }

            }
            else if (db.user_notifications.Where(u => u.user_name == opponentName && u.receiver_name == userName && u.type == 1).Any() && checkUser && checkOpponent && checkSubject)
            {
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }

        [Authorize]
        public ActionResult FindPeople(string term)
        {
            return Json(db.users.Where(u => u.user_name.StartsWith(term) || u.first_name.StartsWith(term) || u.last_name.StartsWith(term)).Select(n => n.user_name).ToList(), JsonRequestBehavior.AllowGet);
        }

    }
}
