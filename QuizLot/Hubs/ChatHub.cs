﻿using System;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using QuizLot.Models;
namespace QuizLot
{
    public class ChatHub : Hub
    {
        public void Send(string userId, string friendId, string message)
        {
            //string name = Context.User.Identity.Name;
            //Clients.All.addChatMessage(name, message);
            //Clients.User(userId).send(userId, message);

            QuizLotEntities db = new QuizLotEntities();
            user_notifications noti = new user_notifications();

            string[] messageForDb = message.Split('|');

            if (string.IsNullOrEmpty(messageForDb[2]))
            {
                noti.user_name = db.users.Find(Convert.ToInt32(userId)).user_name;
                noti.receiver_name = db.users.Find(Convert.ToInt32(friendId)).user_name;
                noti.type = Convert.ToByte(messageForDb[1]);
                noti.message = messageForDb[0];
                noti.date = DateTime.Now;

                db.user_notifications.Add(noti);
                db.SaveChanges();
            }
            else
            {
                noti.user_name = db.users.Find(Convert.ToInt32(userId)).user_name;
                noti.receiver_name = db.users.Find(Convert.ToInt32(friendId)).user_name;
                noti.type = Convert.ToByte(messageForDb[1]);
                noti.message = messageForDb[0];
                noti.subject_name = messageForDb[2];
                noti.date = DateTime.Now;
                db.user_notifications.Add(noti);
                db.SaveChanges();
            }


            var username = db.users.Find(Convert.ToInt32(userId));
            Clients.User(friendId).send(username.user_name.ToString(), message);
        }
    }
}