//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuizLot.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class users
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Username is required")]
        [MinLength(3, ErrorMessage = "Minimum 3 chars in length")]
        public string user_name { get; set; }
        [MinLength(3, ErrorMessage = "Minimum 3 chars in length")]
        public string first_name { get; set; }
        [MinLength(3, ErrorMessage = "Minimum 3 chars in length")]
        public string last_name { get; set; }
        [Required(ErrorMessage = "Email is required")]
        [MinLength(10, ErrorMessage = "Minimum 10 chars in length")]
        [EmailAddress(ErrorMessage = "Is not a valid email")]
        public string email { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [MinLength(3, ErrorMessage = "Minimum 3 chars in length")]
        public string password { get; set; }
        [Required(ErrorMessage = "Gender is required")]
        public byte gender { get; set; }
        public Nullable<int> points { get; set; }
        public System.DateTime date { get; set; }
        public string pro_pic_location { get; set; }
        public string cover_pic_location { get; set; }
    }
}
