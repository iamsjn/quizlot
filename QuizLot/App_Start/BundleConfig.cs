﻿using System.Web;
using System.Web.Optimization;

namespace QuizLot
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                                         "~/Scripts/jquery.js",
                                         "~/Scripts/bootstrap.min.js",
                                         "~/Scripts/placeholder.js",
                                         "~/Scripts/jquery-ui.min.js",
                                         "~/Scripts/respond.min.js",
                                         "~/Scripts/html5shiv.js",
                                         "~/Scripts/flat.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                                        "~/Content/bootstrap.min.css",
                                        "~/Content/font-awesome.min.css",
                                        "~/Content/flat.css",
                                        "~/Content/style.css",
                                        "~/Content/demo.css"));

            bundles.Add(new StyleBundle("~/Content/Profile-css").Include(
                            "~/Content/Profile/profile_pic.css", 
                            "~/Content/Profile/ihover.css", 
                            "~/Content/Profile/profile_rating.css",
                            "~/Content/Profile/style-264.css",
                            "~/Content/Profile/bootstrap.vertical-tabs.css"));

            bundles.Add(new StyleBundle("~/Content/quiz-css").Include(
                        "~/Content/quiz/quiz.css",
                        "~/Content/quiz/quiz-2.css"));

            bundles.Add(new StyleBundle("~/Content/topic-css").Include(
                        "~/Content/topic/topic.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}