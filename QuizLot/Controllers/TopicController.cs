﻿using QuizLot.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuizLot.Controllers
{
    public class TopicController : Controller
    {
        QuizLotEntities db = new QuizLotEntities();
        //
        // GET: /Topic/

        [Authorize]
        public ActionResult Index(string opponentName)
        {
            if (opponentName != Convert.ToString(HttpContext.User.Identity.Name.Split('|')[1]))
            {
                var opponent = db.users.Where(u => u.user_name == opponentName).Any();

                if (opponent == true)
                {
                    dynamic mymodel = new ExpandoObject();

                    int userid = Convert.ToInt32(HttpContext.User.Identity.Name.Split('|')[0]);
                    string username = Convert.ToString(HttpContext.User.Identity.Name.Split('|')[1]);

                    var checkValidity = db.user_notifications.Where(n => n.user_name == username && n.receiver_name == opponentName && n.type == 1).Any();
                    var checkValidity2 = db.user_notifications.Where(n => n.user_name == opponentName && n.receiver_name == username && n.type == 1).Any();

                    if (checkValidity)
                    {
                        ViewBag.validityError = "The user hasn't accepted your previous challenge yet.";
                        return View();
                    }
                    else if (checkValidity2)
                    {
                        ViewBag.validityError = "You haven't accepted this user's previous challenge yet.";
                        return View();
                    }
                    else
                    {
                        //here users model has opponent information
                        mymodel.users = db.users.Where(o => o.user_name == opponentName).ToList();

                        var usersubjectId = db.user_favorite_subjects.Where(u => u.user_id == userid).ToList();
                        List<subjects> templist = new List<subjects>();
                        List<subjects> subjectlist = new List<subjects>();

                        foreach (var sid in usersubjectId)
                        {
                            templist = db.subjects.Where(i => i.id == sid.subject_id).ToList();
                            subjectlist.AddRange(templist);
                        }

                        mymodel.subjects = subjectlist;
                        return View(mymodel);
                    }
                }
                else
                {
                    return HttpNotFound();
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        [Authorize]
        public ActionResult FindPeople(string term)
        {
            return Json(db.users.Where(u => u.user_name.StartsWith(term) || u.first_name.StartsWith(term) || u.last_name.StartsWith(term)).Select(n => n.user_name).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost][Authorize]
        public ActionResult FindTopic(string key)
        {
            return Json(db.subjects.Where(u => u.name.Contains(key)).ToList());
        }

        [HttpPost][Authorize]
        public ActionResult CheckTopic(string topicName, string userId)
        {
            if (db.subjects.Where(u => u.name == topicName).Any())
            {
                int user_id = Convert.ToInt32(userId);
                int subject_id = db.subjects.Where(n => n.name == topicName).Select(i => i.id).FirstOrDefault();

                if (!db.user_favorite_subjects.Where(u => u.user_id == user_id && u.subject_id == subject_id).Any())
                {
                    user_favorite_subjects userSub = new user_favorite_subjects();
                    userSub.user_id = user_id;
                    userSub.subject_id = subject_id;
                    userSub.date = DateTime.Now;

                    db.user_favorite_subjects.Add(userSub);
                    db.SaveChanges();

                    return Json(true);
                }
                else
                {
                    return Json(true);
                }
            }
            else
            {
                return Json(false);
            }
        }

        [Authorize]
        public ActionResult GetNoti(string receiverName)
        {
            return Json(db.user_notifications.Where(r => r.receiver_name == receiverName).OrderByDescending(d => d.date).ToList(), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DeleteNoti(string username, string receivername, string notitype)
        {
            byte type = Convert.ToByte(notitype);

            if (db.user_notifications.Where(u => u.user_name == username && u.receiver_name == receivername && u.type == type).Any())
            {
                var itemToRemove = db.user_notifications.SingleOrDefault(n => n.user_name == username && n.receiver_name == receivername && n.type == type);
                db.user_notifications.Remove(itemToRemove);
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

        }

    }
}
